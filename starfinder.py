#!/usr/bin/env python3
import sys
import argparse
'''
usage: python starfinder.py <gpdb-file> <n> <dmax> <destination-fol>
'''

# Notes:
# The format of a .cliq file (in each line) is as follows:
# <PDB-ID>\t<m1>\t<m2>\t<m3>... \t<mn>\n
#
# ,where mi is the i-th chemical-group to have been discovered considering m1 as the
# central chemical group. i goes from 1 to n such that n is the size of each clique
# In the star-cliques definition that this code follows, all chemical groups m2 to mn, are
# less than dmax distance away from the m1 chemical group. There is only one star-clique
# per chemical group.

def fileread(gfile):
# Reads gpdb file and appends [x, y, z] to values, which is a list
    with open(gfile, 'r') as tfile:
        tmpfile = tfile.readlines()

    values = []
    for i, dat in enumerate(tmpfile):
        if dat[0:6].strip()=="ATOM":
            values.append([dat[31:38].strip(), dat[
                38:46].strip(), dat[46:54].strip()])

    return values


def query(values, n, dmax):
    import scipy.spatial as sp
    import numpy as np
    coor = sp.cKDTree(values)

    # tmpdat is now the whole pdb info and
    # coor is the cKDTree object to be used for query

    n = int(n)
    dmax = float(dmax)
    # dd : array of floats : The distances to the nearest neighbors. If x has shape tuple+(self.m,), then d has shape tuple+(k,). When k == 1, the last dimension of the output is squeezed.
    # ii : ndarray of ints : The locations of the neighbors in self.data. If x has shape tuple+(self.m,), then i has shape tuple+(k,). When k == 1, the last dimension of the output is squeezed.

    dd, ii = coor.query(values, k=n, eps=0,
                        p=2, distance_upper_bound=dmax)
    li = np.ndarray.tolist(ii)      # convert ii to list and name it li

    # print(li)

    # then join all the elements in one line with \t and the lines with \n
    for i, x in enumerate(li):
        li[i] = [str(number) for number in li[i]]
        li[i] = '\t'.join(li[i])
    ki = '\n'.join(li)
    ki = ki.replace(str(len(li)) + '\t', '')
    ki = ki.replace(str(len(li)) + '\n', '\n')

    # print "The max distance found is: ", np.amax(dd)              # uncomment this line to print the max distance between the centre and a neighbor in any of the cliques


    return ki   # ki is just a \n separated string of cliques each of which is a string of tab separated group ids in the clique


def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]


def wcliqfile(gfilepath, ki, n, cliqfolder):
    cliqfilename = cliqfolder + getFileNameWithoutExtension(gfilepath) + '.cliq'
    with open(cliqfilename, 'w') as cliqf:
        ki = getFileNameWithoutExtension(gfilepath) + '\t' + ki.replace('\n', '\n' + getFileNameWithoutExtension(gfilepath) + '\t')
        cliqf.write(ki + '\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    #-i pdbfile -s source_dir -d destination_-b depth
    parser.add_argument("-i", "--input-file", dest='gpdbfilename', required=True, help="[REQUIRED]\
                        Name of gpdb file, For example: /home/path-to-dir/1onc.pdb")
    parser.add_argument("-n", "--size", default='8', dest='size', help="Size of each star in terms\
                        of total number of chemical groups. Default value = 8")
    parser.add_argument("-d", "--dmax", default='20', dest='dmax', help="Max distance of farthest\
                        chemical group from the central chemical group in the star. \
                        Default value = 20")
    parser.add_argument("-c", "--cliqdir", dest='cliqdir', default= './', help="Destination\
                        directory, where the .cliq file will be written to.\
                        Deafult value is current directory")
    args = parser.parse_args()
    apdbfile = args.gpdbfilename
    acdir = args.cliqdir + '/'
    asize = args.size
    admax = args.dmax
    wcliqfile(apdbfile, query(fileread(apdbfile),
                             asize, admax), asize, acdir)



