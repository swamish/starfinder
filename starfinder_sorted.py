import sys
import numpy as np
'''
usage: python starfinder_sorted <gpdb-file> <n> <dmax> <destination-fol>
'''

# Notes:
# The format of a .cliq file (in each line) is as follows:
# <PDB-ID>\t<m1>\t<m2>\t<m3>... \t<mn>\n
#
# ,where mi is the i-th chemical-group to have been discovered considering m1 as the
# central chemical group. i goes from 1 to n such that n is the size of each clique
# In the star-cliques definition that this code follows, all chemical groups m2 to mn, are
# less than dmax distance away from the m1 chemical group. There is only one star-clique
# per chemical group.

def fileread(gfile):
# Reads gpdb file and appends [x, y, z] to values, which is a list
    with open(gfile, 'r') as tfile:
        tmpfile = tfile.readlines()

    values = []
    for i, dat in enumerate(tmpfile):
        if dat[0:6].strip()=="ATOM":
            values.append([dat[31:38].strip(), dat[
                38:46].strip(), dat[46:54].strip()])

    return values


def query(values, n, dmax):
    import scipy.spatial as sp
    coor = sp.cKDTree(values)

    # tmpdat is now the whole pdb info and
    # coor is the cKDTree object to be used for query

    n = int(n)
    dmax = float(dmax)
    # dd : array of floats : The distances to the nearest neighbors. If x has shape tuple+(self.m,), then d has shape tuple+(k,). When k == 1, the last dimension of the output is squeezed.
    # ii : ndarray of ints : The locations of the neighbors in self.data. If x has shape tuple+(self.m,), then i has shape tuple+(k,). When k == 1, the last dimension of the output is squeezed.

    dd, ii = coor.query(values, k=n, eps=0,
                        p=2, distance_upper_bound=dmax)
    li = np.ndarray.tolist(ii)      # convert ii to list and name it li

    # print(li)
    # then join all the elements in one line with \t and the lines with \n
    # for i, x in enumerate(li):
        # li[i] = [str(number) for number in li[i]]
        # li[i] = '\t'.join(li[i])
    # ki = '\n'.join(li)
    # ki = ki.replace(str(len(li)) + '\t', '')
    # ki = ki.replace(str(len(li)) + '\n', '\n')

    # print "The max distance found is: ", np.amax(dd)              # uncomment this line to print the max distance between the centre and a neighbor in any of the cliques


    # return ki   # ki is just a \n separated string of cliques each of which is a string of tab separated group ids in the clique
    return li       # return list of lists, where each list is a clique, in the form of a list of group IDs of groups in that clique

def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]


def wcliqfile(gfilepath, ki, n, cliqfolder):
    with open(gfilepath, 'r') as gfobject:
        gfile = gfobject.readlines()

    pdblist={}
    for i, smgroup in enumerate(gfile):
        if smgroup.startswith("ATOM"):
            pdblist[smgroup[6:11].strip()]=[smgroup[12:16].strip(), smgroup[22:26].strip()]
    # for i in sorted(pdblist.keys()):
        # print(i+" : "+pdblist[i][0])
    maxlen=len(pdblist.keys())
    for i, smclique in enumerate(ki):
        compo=[]
        rescompo=[]
        finalclique=[]
        for j, smgroupid in enumerate(smclique):
            if int(smgroupid)<maxlen:
                compo.append(pdblist[str(smgroupid+1)][0])
                rescompo.append(pdblist[str(smgroupid+1)][1] )
                # centre and then the rest of the groups sorted based on atom id
                compo=[compo[0]]+sorted(compo[1:])
                finalclique.append(str(smgroupid))
            else:
                continue
        compo="_".join(compo)


        if len(finalclique)==float(n):
            if not all(x == rescompo[0] for x in rescompo):
                cliqfilename=cliqfolder + compo+".cliqs"
                with open(cliqfilename, 'a') as cliqf:
                    cliqf.write(getFileNameWithoutExtension(gfilepath) + '\t' +\
                        '\t'.join(finalclique) + '\n')
        else:
            continue


    # cliqfilename = cliqfolder + getFileNameWithoutExtension(gfilepath) + '.cliq'
    # with open(cliqfilename, 'w') as cliqf:
        # ki = getFileNameWithoutExtension(gfilepath) + '\t' + ki.replace('\n', '\n' + getFileNameWithoutExtension(gfilepath) + '\t')
        # cliqf.write(ki)

try:
    wcliqfile(sys.argv[1], query(fileread(sys.argv[1]),
                             sys.argv[2], sys.argv[3]), sys.argv[2], sys.argv[4])
except Exception as e:
    print('Exception: '+str(sys.exc_info()[0]) + "\n" + str(e))
    print(sys.argv[1])


